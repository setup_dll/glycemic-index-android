package gm.pichugin.glycemicindex.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import gm.pichugin.glycemicindex.vo.Product

@Dao
abstract class ProductDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertProduct(product: Product)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertProducts(products: List<Product>)

    @Query("SELECT * FROM Product")
    abstract fun loadProducts(): LiveData<List<Product>>

//    @Query("SELECT * FROM Product WHERE categoryId = :categoryId")
//    abstract fun loadProducts(categoryId: Int): LiveData<List<Product>>

    @Query("SELECT * FROM Product WHERE id = :productId LIMIT 1")
    abstract fun loadProduct(productId: Int): LiveData<Product>
}