package gm.pichugin.glycemicindex.db

import androidx.room.Database
import androidx.room.RoomDatabase
import gm.pichugin.glycemicindex.vo.Product

@Database(
    entities = [
        Product::class
    ], version = 6, exportSchema = false
)
//@TypeConverters(DbTypeConverters::class)
abstract class Database : RoomDatabase() {
    abstract fun productDao(): ProductDao
}