package gm.pichugin.glycemicindex.vo

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Product(
    @PrimaryKey
    val id: Int,
    val category: String,
    val name: String,
    var glycemicIndex: Int,
    var glycemicLoad: Float = 0f,
    val carbs: Float,
    val proteins: Float,
    val fats: Float,
    @SerializedName("kcal") val calories: Float,
    val imageUrl: String
)