package gm.pichugin.protectandroid.binding

import androidx.databinding.BindingAdapter
import android.view.View
import android.widget.TextView
import androidx.databinding.InverseBindingAdapter

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }
}
//
//@BindingAdapter("goneUnless")
//fun goneUnless(view: View, visibility: MutableLiveData<Boolean>?) {
//    val parentActivity: AppCompatActivity? = view.getParentActivity()
//    if (parentActivity != null && visibility != null) {
//        visibility.observe(parentActivity, Observer { value ->
//            val visible = value ?: true
//            view.visibility = if (visible) View.VISIBLE else View.GONE
//        })
//    }
//}
//
//@BindingAdapter("mutableVisibility")
//fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
//    val parentActivity: AppCompatActivity? = view.getParentActivity()
//    if (parentActivity != null && visibility != null) {
//        visibility.observe(parentActivity, Observer { value -> view.visibility = value ?: View.VISIBLE })
//    }
//}
//
//@BindingAdapter("mutableText")
//fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
//    val parentActivity: AppCompatActivity? = view.getParentActivity()
//    if (parentActivity != null && text != null) {
//        text.observe(parentActivity, Observer { value -> view.text = value ?: "" })
//    }
//}
