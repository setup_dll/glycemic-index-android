package gm.pichugin.glycemicindex.util

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import gm.pichugin.glycemicindex.R

class GiUtils(context: Context) {
    // Gl: https://www.healthline.com/nutrition/low-glycemic-diet#section3

    val medium = 56
    val high = 70

    private val colorLow = ContextCompat.getColor(context, R.color.colorGood)
    private val colorMedium = ContextCompat.getColor(context, R.color.colorMedium)
    private val colorHigh = ContextCompat.getColor(context, R.color.colorBad)


    fun getGiColor(index: Int): Int {
        val startColor: Int
        val endColor: Int
        var ratio = 1f

        if (index >= medium) {
            if (index >= high) {
                startColor = colorHigh
                endColor = colorHigh
            } else {
                startColor = colorMedium
                endColor = colorHigh
                ratio = index / high.toFloat()
            }
        } else {
            startColor = colorLow
            endColor = colorMedium
            ratio = index / medium.toFloat()
        }

        return ColorUtils.blendARGB(startColor, endColor, ratio)
    }
}
