package gm.pichugin.glycemicindex.ui.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.android.support.DaggerFragment
import gm.pichugin.glycemicindex.AppExecutors
import gm.pichugin.glycemicindex.R
import gm.pichugin.glycemicindex.binding.FragmentDataBindingComponent
import gm.pichugin.glycemicindex.databinding.FragmentProductListBinding
import gm.pichugin.glycemicindex.util.GiUtils
import gm.pichugin.glycemicindex.util.autoCleared
import javax.inject.Inject

class ProductListFragment : DaggerFragment() {
    @Inject
    lateinit var appExecutors: AppExecutors
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var giUtils: GiUtils

    private val params by navArgs<ProductListFragmentArgs>()
    private var binding by autoCleared<FragmentProductListBinding>()
    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    private lateinit var viewModel: ProductListViewModel
    private var adapter by autoCleared<ProductListAdapter>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<FragmentProductListBinding>(
            inflater,
            R.layout.fragment_product_list,
            container,
            false,
            dataBindingComponent
        )

        binding = dataBinding
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val categoryId = params.categoryId

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductListViewModel::class.java)
        viewModel.setCategoryId(categoryId)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.products = viewModel.products

        val rvAdapter = ProductListAdapter(dataBindingComponent, appExecutors, giUtils) { product ->
            findNavController().navigate(ProductListFragmentDirections.showProduct(product.id))
        }
        binding.productList.adapter = rvAdapter
        this.adapter = rvAdapter

        viewModel.products.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it?.data)
        })
    }
}
