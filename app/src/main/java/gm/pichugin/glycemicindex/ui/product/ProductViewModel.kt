package gm.pichugin.glycemicindex.ui.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import gm.pichugin.glycemicindex.repository.ProductRepository
import gm.pichugin.glycemicindex.vo.Product
import gm.pichugin.glycemicindex.vo.Resource
import javax.inject.Inject

class ProductViewModel @Inject constructor(
    private val repository:  ProductRepository
) : ViewModel() {
    private val _productId = MutableLiveData<Int>()
    val productId: LiveData<Int>
        get() = _productId

    val product: LiveData<Resource<Product>> = Transformations
        .switchMap(_productId) { productId -> repository.loadProduct(productId) }

    fun setProductId(productId: Int) {
        if (_productId.value != productId) {
            _productId.value = productId
        }
    }
}