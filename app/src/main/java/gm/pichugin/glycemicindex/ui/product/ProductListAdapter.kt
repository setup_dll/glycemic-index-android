package gm.pichugin.glycemicindex.ui.product

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import gm.pichugin.glycemicindex.AppExecutors
import gm.pichugin.glycemicindex.R
import gm.pichugin.glycemicindex.databinding.ItemProductBinding
import gm.pichugin.glycemicindex.ui.common.DataBoundListAdapter
import gm.pichugin.glycemicindex.util.GiUtils
import gm.pichugin.glycemicindex.vo.Product


class ProductListAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    private val giUtils: GiUtils,
    private val productClickCallback: ((Product) -> Unit)?
) : DataBoundListAdapter<Product, ItemProductBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<Product>() {
        override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem == newItem
        }
    }
) {
    override fun createBinding(parent: ViewGroup): ItemProductBinding {
        val binding = DataBindingUtil.inflate<ItemProductBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_product,
            parent,
            false,
            dataBindingComponent
        )

        binding.root.setOnClickListener {
            binding.product?.let {
                productClickCallback?.invoke(it)
            }
        }

        return binding
    }

    override fun bind(binding: ItemProductBinding, item: Product) {
        binding.product = item

        val unwrappedDrawable = binding.glycemicIndex.background
        val wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable)

        val color = giUtils.getGiColor(item.glycemicIndex)
        DrawableCompat.setTint(wrappedDrawable, color)
    }
}