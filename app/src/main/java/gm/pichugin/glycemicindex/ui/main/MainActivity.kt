package gm.pichugin.glycemicindex.ui.main

import android.os.Bundle
import dagger.android.support.DaggerAppCompatActivity
import gm.pichugin.glycemicindex.R

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
