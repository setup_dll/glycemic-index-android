package gm.pichugin.glycemicindex.ui.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import gm.pichugin.glycemicindex.repository.ProductRepository
import gm.pichugin.glycemicindex.vo.Product
import gm.pichugin.glycemicindex.vo.Resource
import javax.inject.Inject

class ProductListViewModel @Inject constructor(
    private val repository: ProductRepository
) : ViewModel() {
    private val _categoryId = MutableLiveData<Int>()
    val categoryId: LiveData<Int>
        get() = _categoryId

    val products: LiveData<Resource<List<Product>>> = Transformations
        .switchMap(_categoryId) { categoryId ->
            repository.loadProducts(categoryId)
        }

    fun setCategoryId(categoryId: Int) {
        if (_categoryId.value != categoryId) {
            _categoryId.value = categoryId
        }
    }
}