package gm.pichugin.glycemicindex.ui.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import dagger.android.support.DaggerFragment
import gm.pichugin.glycemicindex.R
import gm.pichugin.glycemicindex.binding.FragmentDataBindingComponent
import gm.pichugin.glycemicindex.databinding.FragmentProductBinding
import gm.pichugin.glycemicindex.util.GiUtils
import gm.pichugin.glycemicindex.util.autoCleared
import javax.inject.Inject

class ProductFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
//    @Inject
//    lateinit var giUtils: GiUtils

    private val params by navArgs<ProductFragmentArgs>()
    private var binding by autoCleared<FragmentProductBinding>()
    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    private lateinit var viewModel: ProductViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<FragmentProductBinding>(
            inflater,
            R.layout.fragment_product,
            container,
            false,
            dataBindingComponent
        )

        binding = dataBinding
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val productId = params.productId

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProductViewModel::class.java)
        viewModel.setProductId(productId)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.product = viewModel.product

//        val unwrappedDrawable = binding.glycemicIndex.background
//        val wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable)
//
//        val color = giUtils.getGiColor(viewModel.product.glycemicIndex)
//        DrawableCompat.setTint(wrappedDrawable, color)
    }
}
