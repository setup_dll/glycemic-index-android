package gm.pichugin.glycemicindex

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import gm.pichugin.glycemicindex.di.DaggerAppComponent

class App : DaggerApplication() {
    private val applicationInjector = DaggerAppComponent.builder()
        .application(this)
        .build()

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = applicationInjector
}