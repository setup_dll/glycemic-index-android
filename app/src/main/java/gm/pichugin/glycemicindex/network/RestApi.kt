package gm.pichugin.glycemicindex.network

import androidx.lifecycle.LiveData
import gm.pichugin.glycemicindex.api.ApiResponse
import gm.pichugin.glycemicindex.vo.Category
import gm.pichugin.glycemicindex.vo.Product
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RestApi {
    @GET("categories")
    fun getCategories(@Path("brandId") brandId: Int): LiveData<ApiResponse<List<Category>>>

    @GET("products")
    fun getProducts(@Query("categoryId") categoryId: Int): LiveData<ApiResponse<List<Product>>>

    @GET("products/{productId}")
    fun getProduct(@Path("productId") productId: Int): LiveData<ApiResponse<Product>>
}