package gm.pichugin.glycemicindex.repository

import androidx.lifecycle.LiveData
import gm.pichugin.glycemicindex.AppExecutors
import gm.pichugin.glycemicindex.db.ProductDao
import gm.pichugin.glycemicindex.db.Database
import gm.pichugin.glycemicindex.network.RestApi
import gm.pichugin.glycemicindex.util.RateLimiter
import gm.pichugin.glycemicindex.vo.Product
import gm.pichugin.glycemicindex.vo.Resource
import java.util.concurrent.TimeUnit
import javax.inject.Inject

const val PRODUCTS_KEY = "PRODUCTS"

class ProductRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val dao: ProductDao,
    private val api: RestApi
) {
    private val productListRateLimit = RateLimiter<String>(10, TimeUnit.MINUTES)

    fun loadProduct(productId: Int): LiveData<Resource<Product>> {
        return object : NetworkBoundResource<Product, Product>(appExecutors) {
            override fun saveCallResult(item: Product) {
                dao.insertProduct(item)
            }

            override fun shouldFetch(data: Product?): Boolean {
                return data == null
            }

            override fun loadFromDb() = dao.loadProduct(productId)

            override fun createCall() = api.getProduct(productId)

            override fun onFetchFailed() {
                productListRateLimit.reset(PRODUCTS_KEY)
            }
        }.asLiveData()
    }

    fun loadProduct(namePart: String) {
    }

    fun loadProducts(categoryId: Int): LiveData<Resource<List<Product>>> {
        return object : NetworkBoundResource<List<Product>, List<Product>>(appExecutors) {
            override fun saveCallResult(item: List<Product>) {
                item.forEach {
                    it.glycemicLoad = it.glycemicIndex * it.carbs / 100
                }
                dao.insertProducts(item)
            }

            override fun shouldFetch(data: List<Product>?): Boolean {
                return data == null || data.isEmpty()
//                        || productListRateLimit.shouldFetch(PRODUCTS_KEY)
            }

//            override fun loadFromDb() = if (categoryId < 0) dao.loadProducts() else dao.loadProducts(categoryId)
            override fun loadFromDb() = dao.loadProducts()

            override fun createCall() = api.getProducts(categoryId)

            override fun onFetchFailed() {
                productListRateLimit.reset(PRODUCTS_KEY)
            }
        }.asLiveData()
    }
}