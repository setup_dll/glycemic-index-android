package gm.pichugin.glycemicindex.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import gm.pichugin.glycemicindex.ui.product.ProductFragment
import gm.pichugin.glycemicindex.ui.product.ProductListFragment

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeProductFragment(): ProductFragment

    @ContributesAndroidInjector
    abstract fun contributeProductListFragment(): ProductListFragment
}