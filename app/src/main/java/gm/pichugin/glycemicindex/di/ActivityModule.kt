package gm.pichugin.glycemicindex.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import gm.pichugin.glycemicindex.ui.main.MainActivity

@Suppress("unused")
@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity
}