package gm.pichugin.glycemicindex.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import gm.pichugin.glycemicindex.db.Database
import gm.pichugin.glycemicindex.db.ProductDao
import gm.pichugin.glycemicindex.network.RestApi
import gm.pichugin.glycemicindex.util.GiUtils
import gm.pichugin.glycemicindex.util.LiveDataCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
class AppModule {
    /**
     * Provides the REST service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the REST service implementation.
     */
    @Singleton
    @Provides
    fun provideApplicationApi(retrofit: Retrofit): RestApi {
        return retrofit.create(RestApi::class.java)
    }

    /**
     * Provides the OkHttpClient object.
     * @return the OkHttpClient object
     */
    @Singleton
    @Provides
    fun provideHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().apply {
            this.addInterceptor(HttpLoggingInterceptor().apply {
                this.level = HttpLoggingInterceptor.Level.BASIC
            })
        }.build()
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Singleton
    @Provides
    fun provideRetrofitInterface(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://5cac2bc3c85e05001452efeb.mockapi.io")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .client(client)
            .build()
    }

    @Singleton
    @Provides
    fun provideGiUtils(application: Application): GiUtils {
        return GiUtils(application)
    }

    @Singleton
    @Provides
    fun provideDatabase(application: Application): Database {
        return Room
            .databaseBuilder(application, Database::class.java, "glycemicIndex.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideProductDao(database: Database): ProductDao {
        return database.productDao()
    }
}